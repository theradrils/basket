<?php

class Vue {

  private $fichier;
  private $sourceGabarit;

  public function __construct($action, $controleur = "") {
    // Determination du nom du fichier vue a partir de l'action et du constructeur
    $fichier  = ROOT . "/Vue/";
    if ($controleur != "") {
      $fichier  =  $fichier . $controleur . "/";
    }//end if
    $this->fichier =  $fichier . $action . '.php';
    $this->setGabarit($controleur);
  }// end construct

  public function generer( $donnees = []) {

    $contenu = $this->genererFichier($this->fichier, $donnees);
    $racineWeb  = Configuration::get("racineWeb", "/");
    $vue = $this->genererFichier($this->sourceGabarit, array('contenu' => $contenu, 'racineWeb' => $racineWeb));
    echo $vue;
  }//end function

  private function genererFichier($fichier, $donnees) {

      if (file_exists($fichier)) {
          extract($donnees);
          ob_start();
          require $fichier;
          return ob_get_clean();
      }else{
          throw new Exception("fichier $fichier introuvable");
      }//end if
  }// end function

  private function nettoyer($valeur) {
    return htmlspecialchars($valeur, ENT_QUOTES, 'UTF-8', false);
  }// end function

  private function setGabarit($controleur) {
    $this->sourceGabarit  =   ROOT . '/Vue/templates/gabarit.php';
    if($controleur == 'Admin'){
      $this->sourceGabarit  = ROOT . '/Vue/Admin/templates/gabaritAdmin.php';
    }
  }

}
?>
