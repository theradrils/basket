<?php

Class Configuration {

  private static $parametres;

    //Renvoie la valeur d'un parametre de Configuration
    public static function get($nom, $valeurParDefaut = null) {
      if (isset(self::getParametres()[$nom])){
        $valeur = self::getParametres()[$nom];
      }else{
        $valeur = $valeurParDefaut;
      }//end if
      return $valeur;
    }

    //renvoie le tableau des paramétres en le chargeant au besoin
    private static function getParametres() {

      if (self::$parametres == null) {
        $cheminFichier = ROOT . "/Config/prod.ini";
        if (!file_exists($cheminFichier)) {
          $cheminFichier = ROOT . "/Config/dev.ini";
        }//end if
        if (!file_exists($cheminFichier)) {
          throw new Exception("Aucun fichier de configuration trouvé");
        }else{
          self::$parametres = parse_ini_file($cheminFichier);
        }//end if
      }//end if

      return self::$parametres;
    }
}


?>
