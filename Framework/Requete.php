<?php


Class Requete {

  // Parametres de la Requete.
  private $parametres;

  public function __construct($parametres) {
    $this->parametres = $parametres;


  }//end __construct

  // Renvoie vrai si le parametre existe dans la requete.
  public function existeParametre($nom) {
    return (isset($this->parametres[$nom]) && $this->parametres[$nom] != "");

  }//end function

  // Renvoie la valeur du parametre demandé
  // Leve une exception si le parametre est introuvable
  public function getParametre($nom) {

    if ($this->existeParametre($nom)) {
      return $this->parametres[$nom];
    }else{
      throw new Exception("Parametre '$nom' absent de la requete");
    }//end if
  }//end function
}//end class



?>
