<?php

abstract Class Controleur {

	// Action a realiser
	private $action;

	// Requete entrante
	protected $requete;

	// Definit la requete entrante
	public function setRequete(Requete $requete) {
		$this->requete	=	$requete;
	}//end function

	// Execute l'action a realiser
	public function executerAction($action) {
		if (method_exists($this, $action)) {
			$this->action	=	$action;
			$this->{$this->action}();
		}else{
			$classeControleur		=	get_class($this);
			throw new Exception("Action '$action' non definie dans la classe $classeControleur");

		}//end if
	}//end function

	// Methode abstraite correspondant a l'action par defaut
	// Oblige les classes derivées a implementer cette action par defaut
	public abstract function index();

	// Genere la vue associée au controleur courant
	protected function genererVue($donneesVue	=	array()) {
		$classeControleur	=	get_class($this);
		$controleur	=	str_replace("Controleur", "", $classeControleur);
		// Instanciation et generation de la vue
		$vue = new Vue($this->action, $controleur);
		$vue->generer($donneesVue);
	}// end function
}

?>
