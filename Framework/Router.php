<?php

class Router {

  /**
   * Route une requete entrante : execute l'action associee
   */
  public function routerRequete() {
    try {
      // Fusion des parametres GET et POST de la Requete
      $requete      = new Requete(array_merge($_GET, $_POST));
      $controleur   = $this->creerControleur($requete);
      $action       = $this->creerAction($requete);

      $controleur->executerAction($action);
    }catch (Exception $e) {
      $this->gererErreur($e);
    }//end try catch
  }//end function


  /**
   * Creer le controleur approprié en fonction de la requete reçu
   * @param  Requete $requete
   * @return Object retourn la classe controleur a appeler
   */
  private function creerControleur(Requete $requete) {
    $controleur   = "Accueil";  // Controleur par defaut
    if ($requete->existeParametre('controleur')) {
      $controleur   = $requete->getParametre('controleur');
      // Premiere lettre en majuscule
      $controleur = ucfirst(strtolower($controleur));
    }//end if
    // Creation du nom du fichier du Controleur
    $classeControleur   = "Controleur" . $controleur;
    $fichierControleur  = ROOT . "/Controleur/" . $classeControleur . ".php";
    if (file_exists($fichierControleur)) {
      // Instanciation du controleur adapté a la Requete
      $controleur = new $classeControleur();
      $controleur->setRequete($requete);
      return $controleur;
    }else{
      throw new Exception ("Fichier '$fichierControleur' introuvable");
    }//end if

  }//end function


  /**
   * Détermine l'action a executer en fonction de la requete reçue
   * @param  Requete
   * @return string
   */
  private function creerAction(Requete $requete) {
    $action = "index";  // Action par defaut
    if ($requete->existeParametre('action')) {
      $action   = $requete->getParametre('action');
    }//end if
    return $action;
  }//end function


  /**
   * Gere une erreur d'execution (exception)
   * @param  Exception $exception [recupere les erreur]
   * @return HTML renvoi la vue d'erreur
   */
  private function gererErreur(Exception $exception) {
    $vue    = new Vue('erreur');
    $vue->generer(array('msgErreur' => $exception->getMessage()));
  }//end function

}//end class




?>
