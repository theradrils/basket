<?php

	class CheminAbsolut {

		// recupere le chemin du fichier qui l'appel. Remonte de un dossier et renvoie le chemin
		public static function lancement() {
			$chemin;
			$realPath = __DIR__;
			$path = explode('/', $realPath);
			$chemin = (array_slice($path, 0, count($path)-1));
			$chemin = implode('/', $chemin);
			return $chemin;
		}
	}

 ?>
