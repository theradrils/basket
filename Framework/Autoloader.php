<?php
require_once(ROOT.'/Framework/Path.php');

class Autoloader {

  public function __construct() {
    $this->register();
  }

  public function register() {
    spl_autoload_register(array(__CLASS__, 'autoload'));
  }

  public function autoload($class) {
    $chemin = $this->getUrl($class . '.php');
    require_once  $chemin;
  }

  private function getUrl($class) {
    $path = new Path();
    foreach ($path->fichier as $key) {
      $fichier = explode('/', $key);
      if (end($fichier) == $class) {
        $chemin =  $key;
        return $chemin;
      }//end if
    }//end foreach
  }//end function
}//end class
