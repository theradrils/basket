<?php
/**
 * Classe abstraite Modèle.
 * Centralise les services d'accès a une base de donnéelse
 * Utilise l'API PDO de php
 *
 * @version 1.0
 * @author Baptiste Pesquet
 */


abstract class Modele {

  /**
   * Object PDO d 'accès a la BDD.
   * Statique dons partagé par toutes les instances des classes dérivées
   */
  private static $bdd;

  /**
   * Execute une requéte SQL
   *
   * @param  string $sql   Requete SQL
   * @param  array $param Parametres de la Requete
   * @return PDOStatement Resultat de la requete
   */
  protected function execRequete($sql, $params = null) {

    if($params == null) {
      $resultat = self::getBdd()->query($sql);    //execution directe
    }else {
      $resultat = self::getBdd()->prepare($sql); //requete preparée
      $resultat->execute($params);
    }//end if
    return $resultat;

  }//end function
  /**
   * Renvoie un objet de connexion a la BDD en initialisant la connexion au besoin
   * @return PDO OBJET PDO de connexion a la BDD
   */
  private static function getBdd() {

    if (self::$bdd === null) {
      // Recuperation des parametres de configuration BadMethodCallException
      $dsn      = Configuration::get('dsn');
      $login    = Configuration::get('login');
      $mdp      = Configuration::get('mdp');
      // Creation de la connexion
      self::$bdd = new PDO($dsn, $login, $mdp,
              array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    }
    return self::$bdd;
  }//end function

}//fin class



?>
