<?php

class ControleurBillet extends Controleur{

  private $billetSelected;
  private $id_billet;

  public function __construct() {

    $this->billetSelected   = new BilletSelected();
  }


  public function index() {
    $id_billet    = $this->requete->getParametre('id');
    $billet       = $this->billetSelected->getBillet($id_billet);
    $rencontre    = $this->billetSelected->getRencontre($id_billet);
    $commentaire  = $this->billetSelected->getCommentaire($id_billet);

    $this->genererVue( array(
      'billet'          => $billet,
      'rencontre'       => $rencontre,
      'commentaire'     => $commentaire
    ));
  }

}



?>
