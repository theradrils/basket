<?php

class ControleurAccueil extends Controleur {

  private $allBillet;

  public function __construct() {

    $this->allBillet    = new Billet();
  }

  public function index() {

    $carroussel     = $this->allBillet->getBillet();
    $this->genererVue( array('carroussel' => $carroussel) );
  }

}



?>
