PROG = {

  champ     : [],
  tablo     : [],
  cible     : [],
  tbl_idx_val : {},

  // ecoute du bouton envoie et recuperation de la valeur du champ input
  init : function() {

    PROG.iniTablo();
    var btn     = document.querySelectorAll('button[name=modifier]');
    var ajout   = document.querySelector('button[name=ajouter]');
    ajout.addEventListener('click', PROG.ajout);
    btn.forEach(index => index.addEventListener('click', PROG.action));
  },

    action : function(e) {
        colonne = document.querySelector('.droite').children;
        var divDroite = e.target.parentNode.parentNode.children[1].children;
        for(i = 0; i < divDroite.length; i++) {
            PROG.tbl_idx_val["c".concat(i)] = {'index' : colonne[i].innerHTML, 'contenu' : divDroite[i].innerHTML};
        }
        PROG.envoi();
    },

    ajout : function() {
        var bordure = document.querySelector('.bordure');
        bordure = bordure.cloneNode(true);
        var patron = document.querySelectorAll('.contenu_table')[0];
        var essai = patron.cloneNode(true);
        test = essai.querySelector('.droite');
        gauche = essai.querySelector('.gauche');
        gauche.removeChild(gauche.children[1]);
        gauche.children[0].children[0].setAttribute('name', 'ajouter');
        gauche.children[0].children[0].innerHTML = "Valider";
        for (var i = 0; i < test.children.length; i++) {
            test.children[i].innerHTML = 'Text a editer';
        }
        test.children[0].innerHTML = 'Non editable';
        test.children[0].setAttribute('contenteditable', 'false');
        document.querySelector('.contenu_all').appendChild(bordure);
        document.querySelector('.contenu_all').appendChild(essai);
        gauche.children[0].children[0].addEventListener('click', PROG.enregistrer);
    },

    iniTablo : function() {

        var divDroite = document.querySelectorAll('.contenu_table .droite');
        for(i = 0; i < divDroite.length; i++) {
          tbl = [];
            for (j = 0; j < divDroite[i].children.length; j++) {
              tbl.push(divDroite[i].children[j].innerHTML);
          }
          PROG.tablo.push([tbl]);
        }
    },

    comparer : function(targetNbr) {

        origine = PROG.tablo[targetNbr].toString();
        cible = PROG.cible.toString();

        if (origine == cible) {
            console.log('BRAVO');
        }else{
            PROG.envoi(cible);
        }
    },

  // Envoie des données a php si le champ est remplie
  envoi : function(cible) {

      var xhttp = new XMLHttpRequest();
      xhttp.onload = function() {

        var retour = xhttp.responseText;
        console.log(retour);
    }

      xhttp.open("POST", "index.php?action=admin&table=equipes", true);
      xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      contenue = JSON.stringify(PROG.tbl_idx_val);
      var envoi =  'valeur='.concat(contenue);
      xhttp.send(envoi);

  },

  // enregistrer : function(cible) {
  //
  //     var xhttp = new XMLHttpRequest();
  //     xhttp.onload = function() {
  //
  //       var retour = xhttp.responseText;
  //       console.log(retour);
  //   }
  //
  //     xhttp.open("POST", "index.php?action=admin&table=equipes", true);
  //     xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  //     var envoi =  'valeur='.concat(PROG.tbl_idx_val);
  //     xhttp.send(envoi);
  //
  // }
}
window.onload = PROG.init;
