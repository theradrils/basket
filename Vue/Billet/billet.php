<section>

  <div class="billet">
    <?php foreach ($billet as $key): ?>
      <article class="billet<?= $key['id_billet'] ?>">
        <header>
          <h3><?= $key['titre_billet'] ?></h3>
        </header>
        <div class="container">
          <p><?= $key['descriptif'] ?></p>
        </div>
      </article>
    <?php endforeach; ?>
  </div>


  <div class="rencontre">
    <?php foreach ($rencontre as $key): ?>
      <article class="rencontre<?= $key['id_rencontres'] ?>">
        <header>
          <h3><?= $key['date_rencontres'] ?></h3>
        </header>
        <div class="container">
          <p><?= $key['equipe_dom'] ?> <?= $key['equipe_ext'] ?> : <?= $key['score'] ?></p>
        </div>
      </article>
    <?php endforeach; ?>
  </div>


  <div class="commentaire">
    <?php foreach ($commentaire as $key): ?>
      <article class="commentaire<?= $key['id_commentaire'] ?>">
        <header>
          <h3><?= $key['titre_commentaire'] ?></h3>
          <p><?= $key['date_commentaire'] ?></p>
        </header>
        <div class="container">
          <p><?= $key['auteur'] ?></p>
          <p><?= $key['contenu'] ?></p>
        </div>
      </article>
    <?php endforeach; ?>
  </div>


</section>
