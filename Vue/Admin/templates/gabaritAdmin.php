<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Association Basket</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/adminModif.js" charset="utf-8"></script>
  </head>
  <body>
    <header>

        <nav>
          <ul>
            <li id="imageLi"> <img src="Images/logod.png" alt=""> </li>
            <li><a href="index.php?action=accueil">Accueil</a></li>
            <li><a href="index.php?action=presentation">Présentation</a></li>
            <li><a href="index.php?action=contact">Contact</a></li>
            <li><a href="index.php?action=connexion">Admin</a></li>
          </ul>
        </nav>

      <div class="image">
        <img src="Images/header.jpeg" alt="">
      </div>

    </header>

    <nav id="nom_table">
      <ul>
          <?php foreach ($allTable as $key): ?>
              <li><a href="index.php?controleur=index&action=index&table=<?php echo $key['table_name'] ?>"><?php echo ucfirst($key['table_name']);  ?></a></li>

          <?php endforeach; ?>
      </ul>
    </nav>

    <div class="main">



        <?= $contenu; ?>
    </div>

    <footer></footer>

  </body>
</html>
