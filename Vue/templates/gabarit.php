
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <base href="<?= $racineWeb; ?>" >
    <title>Association Basket</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/carroussel.js" charset="utf-8"></script>
    <script src="js/tailleItemParagraphe.js" charset="utf-8"></script>

  </head>
  <body>
    <header>

        <nav>
          <ul>
            <li id="imageLi"> <img src="Images/logod.png" alt=""> </li>
            <li><a href="index.php?controleur=accueil">Accueil</a></li>
            <li><a href="index.php?action=presentation">Présentation</a></li>
            <li><a href="index.php?action=contact">Contact</a></li>
            <li><a href="index.php?controleur=admin">Admin</a></li>
          </ul>
        </nav>

      <div class="image">
        <img src="Images/header.jpeg" alt="">
      </div>

    </header>

    <div class="main">
      <?= $contenu; ?>
    </div>

    <footer></footer>

  </body>
</html>
