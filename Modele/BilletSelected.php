<?php

class BilletSelected extends Modele {

  public function getRencontre($id_billet) {

    $sql = "SELECT r.*
    FROM rencontres r
    JOIN billet_rencontre b
    ON b.id_rencontres = r.id_rencontres
    WHERE b.id_rencontres =?
    ";

    $reponse = $this->execRequete($sql, array($id_billet));
    return $reponse;
  }

  public function getBillet($id_billet) {

    $sql = "SELECT b.*
    FROM billet_rencontre b
    WHERE b.id_billet =?
    ";
    $reponse = $this->execRequete($sql, array($id_billet));
    return $reponse;
  }

  public function getCommentaire($id_billet) {

    $sql = "SELECT c.*
    FROM commentaire c
    JOIN billet_rencontre b
    ON b.id_billet = c.id_billet
    WHERE b.id_billet =?
    ";
    $reponse = $this->execRequete($sql, array($id_billet));
    return $reponse;
  }

}

?>
