<?php

class Admin extends Modele {

  public function getUser($pseudo) {

    $sql = "SELECT admin.nom as prenom, admin.password as mdp
    FROM admin
    WHERE admin.nom = :pseudo
    ";
    $reponse = $this->execRequete($sql, array(
      'pseudo' => $pseudo
    ));

     return $reponse->fetch(PDO::FETCH_ASSOC);
  }

  public function getAllTable() {
      $sql = "  SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND table_schema = 'basket' ";
      $reponse = $this->execRequete($sql);
      return $reponse->fetchAll(PDO::FETCH_ASSOC);
  }

  public function getTable($table) {
      $sql = "SELECT * FROM $table";
      $reponse = $this->execRequete($sql);
      return $reponse->fetchAll(PDO::FETCH_ASSOC);
  }

  public function supprLigne($table, $id) {

      $sql = "DELETE FROM $table WHERE $id[0] = :id";
      $reponse = $this->execRequete($sql,
      array("id" => $id[1]
     ));
  }

  public function modifierTable($tbl, $param) {
      print_r($param);
       $id_column = $param->c0->index;
       $id = $param->c0->contenu;
      $colonne = '';
      foreach ($param as $key => $val) {
         if ($key != "c0"){
             $colonne .= $val->index . " = '" . $val->contenu . "', ";
         }
      }
      $colonne = substr($colonne, 0, -2);
      print_r($colonne);
      $sql = "UPDATE $tbl
              SET $colonne
              WHERE $id_column = $id";

    $reponse = $this->execRequete($sql);
}
}


?>
