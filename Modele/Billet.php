<?php

class Billet extends Modele {

  public function getBillet() {

    $sql = 'SELECT id_billet
            AS id_news, descriptif
            AS contenu, titre_billet
            AS titre, img
            AS image
            FROM billet_rencontre
            ORDER BY id_billet DESC';

    $reponse = $this->execRequete($sql);

    return $reponse;

  }

}

 ?>
